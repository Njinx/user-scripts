# UserScripts

Various userscripts created by me to be used with the Grease/Tamper/ViolentMonkey browser extensions. **I AM NOT RESPONSIBLE IF ANY OF THESE SCRIPTS BREAK SOMETHING**

### Scripts
- **MinecraftAccountCreationDate** - Shows the exact date and time you created your Minecraft account at at the bottom of [https://www.minecraft.net/en-us/profile](https://www.minecraft.net/en-us/profile). (Only works for the English site)
