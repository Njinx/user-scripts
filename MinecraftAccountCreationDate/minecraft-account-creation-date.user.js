// ==UserScript==
// @name         Minecraft Account Creation Date
// @namespace   Violentmonkey Scripts
// @match       https://www.minecraft.net/en-us/profile
// @grant       none
// @version     1.0
// @license     BSD
// @homepageURL https://gitlab.com/Njinx/user-scripts
// @supportURL  https://gitlab.com/Njinx/user-scripts/-/issues
// @downloadURL https://gitlab.com/Njinx/user-scripts/-/raw/main/MinecraftAccountCreationDate/minecraft-account-creation-date.user.js
// @author      Njinx
// @description Adds account creation date to the profile section of Minecraft.net
// @run-at      document-idle
// ==/UserScript==

dateCreated = ""

var intervalID = setInterval(cb, 250);

function cb() {
  const profileAnchor = document.querySelector("[data-testid='Profile Name']")
  if (profileAnchor === null) {
    return;
  }
  clearInterval(intervalID);
  
  const profileInfoTable = profileAnchor.parentNode.parentNode.parentNode; // this is very robust code trust me bro.

  divContainer = document.createElement("div");
  profileInfoTable.appendChild(divContainer);
  
  divContainer.classList.add("py-3");
  divContainer.classList.add("border-bottom");
  
  row = document.createElement("dl");
  divContainer.appendChild(row);
  
  row.classList.add("row");
  row.classList.add("align-items-center");
  row.classList.add("mb-0");
  row.classList.add("py-2");
  
  dateCreatedKey = document.createElement("dt");
  row.appendChild(dateCreatedKey);
  
  dateCreatedKey.classList.add("col-4");
  dateCreatedKey.classList.add("font-weight-normal");
  dateCreatedKey.classList.add("text-small");
  dateCreatedKey.classList.add("text-600");
  dateCreatedKey.classList.add("mb-0");
  dateCreatedKey.innerText = "Date Created";

  dateCreatedVal = document.createElement("dd");
  row.appendChild(dateCreatedVal);
  
  dateCreatedVal.classList.add("col-8");
  dateCreatedVal.classList.add("text-small");
  dateCreatedVal.classList.add("text-gray-dark");
  dateCreatedVal.classList.add("mb-0");
  dateCreatedVal.classList.add("d-inline-flex");
  dateCreatedVal.innerText = dateCreated;
}


const bearerToken = document.cookie
  .split('; ')
  .find(row => row.startsWith('bearer_token='))
  .split('=')[1];

fetch("https://api.minecraftservices.com/minecraft/profile/namechange", {
    "credentials": "include",
    "headers": {
        "User-Agent": window.navigator.userAgent,
        "Accept": "*/*",
        "Accept-Language": "en-US,en;q=0.5",
        "Authorization": "Bearer " + bearerToken,
        "Sec-Fetch-Dest": "empty",
        "Sec-Fetch-Mode": "cors",
        "Sec-Fetch-Site": "cross-site",
        "Sec-GPC": "1",
        "Pragma": "no-cache",
        "Cache-Control": "no-cache"
    },
    "referrer": "https://api.minecraftservices.com/",
    "method": "GET",
    "mode": "cors"
}).then(resp => resp.json()).then(data => {
  rawDate = data["createdAt"];
  dateCreated = new Date(rawDate).toLocaleString();
});
